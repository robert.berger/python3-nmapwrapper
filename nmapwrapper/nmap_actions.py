from . import *

correct_targets = ["host", "port"]
correct_actions = ["discovery", "scan"]

class NMAPActions(object):
    def __init__(self, target, action, server=None):
        self.target = target.lower()
        self.action = action.lower()
        self.server = server

    def check_target(self):
        if self.target not in correct_targets:
            raise Exception("Requested Target Not Supported ... %s", self.target)

    def check_action(self):
        if self.action not in correct_actions:
            raise Exception("Requested Action Not Supported ... %s", self.action)

    def nmap_open_ports(self):
        from .scan_open_ports import listening_ports_for_server
        listening_ports = listening_ports_for_server( self.server )

        return listening_ports  


    def nmap_host_discovery(self):
        from .discover_active_hosts import open_hosts_now
        hosts = open_hosts_now()

        return hosts  

    def call_nmap_target_action(self):
        self.check_target()
        self.check_action()

        if self.target == "host" and self.action == "discovery":
            return self.nmap_host_discovery()
        elif self.target == "port" and self.action == "scan" and self.server is not None:
            return self.nmap_open_ports()
        else:
            raise Exception("No Rule for Target %s && Action %s && Server %s", self.target, self.action, self.server)
        