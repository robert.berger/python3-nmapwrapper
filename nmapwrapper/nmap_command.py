
from . import *

import subprocess

####
# Run a linux command and parse command output with generic rules ... like split "\n"
####
class NMapCommandInSubprocess(object):
    def __init__(self, command_str):
        self.command_str = command_str

    # Pthon will open a shell and execute the command inside the shell. The call will return 
    # when the shell finishes. 
    def run_linux_command_blocking(self):
        result = subprocess.run( [self.command_str], shell=True, stdout=subprocess.PIPE )
        self.result_utf8 = result.stdout.decode('utf-8')

        #logger.debug("*** %s", self.command_str)
        #logger.debug("??? %s", self.result_utf8)

    # Return string containing the linux command response after execution.
    def __str__(self):
        return self.result_utf8

    # Split nmap lengthy string output containing answers for all nodes 
    def get_list_of_answers_for_examined_nodes(self):
        rlines = self.result_utf8.split("\n")
        return rlines    

### End of Linux Command in Subprocess
"""
def host_discovery( command = HOST_DISCOVERY_DICT['command'], network_cidr = HOST_DISCOVERY_DICT['cidr'] ):
    result = subprocess.run(
        [ self.command + network_cidr], shell=True, stdout=subprocess.PIPE)
        
    result_utf8 = result.stdout.decode('utf-8')
    
    #logger.debug("******** HOST DISCOVERY =\n%s", result_utf8)
    rlines = result_utf8.split("\n")

    hostsUp_arr = []
    for i in range( 0, len(rlines) ):
        #logger.debug("+++++++++++++++ %s", rlines[i])
        if rlines[i].strip().startswith('Host is up'):
            # ***************
            #  Found Host Up
            # ***************
            #logger.debug("************************* #%d: \n\t %s \n\t %s", i, rlines[i], rlines[i-1])

            hostsUp_arr.append( rlines[i-1] )
    
    # How many Hosts-Up found ?
    logger.debug("Number of Hosts-Up Found = %d", len(hostsUp_arr))

    return hostsUp_arr

def port_scanning( command = PORT_SCANNING_COMMAND, server_ip="192.168.5.60" ):
    logger.debug( "Inside Port Scanning Module for Live Server ... %s", server_ip )

    result = subprocess.run([SCAN_TCP + server_ip], shell=True, stdout=subprocess.PIPE)
    result_utf8 = result.stdout.decode('utf-8')
    
    logger.debug("********PORT SCANNING =\n%s", result_utf8)

    result_lines = result_utf8.split("\n")

    #logger.debug("result_lines = %s", result_lines[0])
""" 
